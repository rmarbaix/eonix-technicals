﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Circus.Models
{
    public class Monkey
    {
        #region Members

        public string Name { get; }
        public List<Trick> Tricks { get; }
        public event Action<Trick, string> OnTrickDone;

        #endregion

        public Monkey(string name, List<Trick> tricks)
        {
            Name = name;
            Tricks = tricks;
        }

        public void DoTrick(Trick trick)
        {
            if (!Tricks.Any(t => t.Equals(trick)))
                return;

            Console.WriteLine($"{Name} exécute le tour {trick.Name}");
            OnTrickDone?.Invoke(trick, Name);
        }
    }
}
