﻿using Circus.Enums;

namespace Circus.Models
{
    public class Trick
    {
        public string Name { get; set; }
        public TrickType TrickType { get; set; }
    }
}
