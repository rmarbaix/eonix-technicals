﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Circus.Models
{
    public class Tamer
    {
        private Monkey _monkey { get; set; }

        public Tamer(Monkey monkey)
        {
            _monkey = monkey;
        }

        public void OrderTricks()
        {
            if (_monkey?.Tricks != null)
            {
                _monkey.Tricks.ForEach(t => _monkey.DoTrick(t));
            }
        }
    }
}
