﻿using Circus.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Circus.Models
{
    public class Spectator
    {
        public string Name { get; }

        public Spectator(string name)
        {
            Name = name;
        }

        public void ShowTrick(Trick trick, string monkeyName)
        {
            Console.ForegroundColor = ConsoleColor.Green;

            switch (trick.TrickType)
            {
                case TrickType.Acrobatics:
                    Console.WriteLine($"{Name} applaudit pendant le tour d'acrobatie '{trick.Name}' de {monkeyName}");
                    break;
                case TrickType.Musical:
                    Console.WriteLine($"{Name} siffle pendant le tour de musique '{trick.Name}' de {monkeyName}");
                    break;
            }

            Console.ForegroundColor = ConsoleColor.White;
        }
    }
}
