﻿using Circus.Models;
using System;
using System.Collections.Generic;

namespace Circus
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Welcome to EONIX Circus");

            Spectator spectator = new Spectator("Raphaël");

            List<Trick> tricksForKing = new List<Trick>() { 
                new Trick() { Name = "Marcher sur les mains" , TrickType = Enums.TrickType.Acrobatics },
                new Trick() { Name = "Jouer de la batterie" , TrickType = Enums.TrickType.Musical },
                new Trick() { Name = "Jouer de la basse" , TrickType = Enums.TrickType.Musical }
            };

            List<Trick> tricksForKong = new List<Trick>() {
                new Trick() { Name = "Faire un salto arrière" , TrickType = Enums.TrickType.Acrobatics },
                new Trick() { Name = "Jouer de la guitare" , TrickType = Enums.TrickType.Musical }
            };

            Monkey king = new Monkey("King", tricksForKing);
            Monkey kong = new Monkey("Kong", tricksForKong);

            // attach
            king.OnTrickDone += spectator.ShowTrick;
            kong.OnTrickDone += spectator.ShowTrick;


            Tamer bouglione = new Tamer(king);
            Tamer phineas = new Tamer(kong);

            bouglione.OrderTricks();
            phineas.OrderTricks();

            // detach
            king.OnTrickDone += spectator.ShowTrick;
            kong.OnTrickDone += spectator.ShowTrick;
        }
    }
}
