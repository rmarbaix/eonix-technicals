﻿using Microsoft.EntityFrameworkCore;
using Person.Infrastructure.EntityConfigurations;
using Person.Domain.Entities;

namespace Person.Infrastructure
{
    public class PersonContext : DbContext
    {
        public const string DEFAULT_SCHEMA = "person_schema";
        public DbSet<Person.Domain.Entities.Person> Persons { get; set; }

        public PersonContext(DbContextOptions options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new PersonConfiguration());
            //base.OnModelCreating(modelBuilder);
        }
    }
}
