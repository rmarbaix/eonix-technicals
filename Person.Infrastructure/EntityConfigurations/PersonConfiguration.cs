﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Person.Infrastructure.EntityConfigurations
{
    internal class PersonConfiguration : IEntityTypeConfiguration<Person.Domain.Entities.Person>
    {
        public void Configure(EntityTypeBuilder<Person.Domain.Entities.Person> personConfiguration)
        {
            personConfiguration.ToTable("persons", PersonContext.DEFAULT_SCHEMA);
            // set the primary key
            personConfiguration.HasKey(o => o.Id);
            // set mandatory fields
            personConfiguration.Property(p => p.FirstName).IsRequired();
            personConfiguration.Property(p => p.LastName).IsRequired();
        }
    }
}
