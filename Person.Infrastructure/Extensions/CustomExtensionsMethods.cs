﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Person.Infrastructure.Extensions
{
    public static class CustomExtensionsMethods
    {
        public static IServiceCollection AddInfrastructure(this IServiceCollection services)
        {
            services.AddDbContext<PersonContext>(opt => opt.UseInMemoryDatabase("person_database"));
            services.BuildServiceProvider().GetRequiredService<PersonContext>().Database.EnsureCreated();

            return services;
        }
    }
}
