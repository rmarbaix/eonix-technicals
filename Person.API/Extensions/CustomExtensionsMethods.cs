﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;

namespace Person.API.Extensions
{
    public static class CustomExtensionsMethods
    {
        public static IServiceCollection AddCustomSwagger(this IServiceCollection services)
        {
            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "EONIX - Persons HTTP API",
                    Version = "v1",
                    Description = "The Person Service HTTP API"
                });
            });

            return services;
        }
    }
}
