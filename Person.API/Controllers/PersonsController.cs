﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Person.API.Application;
using Person.API.Application.Commands;
using Person.API.Application.Queries;
using System;
using System.Threading.Tasks;

namespace Person.API.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class PersonsController : ControllerBase
    {

        #region Members

        private readonly ILogger<PersonsController> _logger;
        private readonly IMediator _mediator;

        #endregion

        #region ctor

        public PersonsController(
            ILogger<PersonsController> logger,
            IMediator mediator)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        }

        #endregion

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] CreateNewPersonCommand command)
        {
            _logger.LogInformation($"{nameof(Create)} - Start");

            PersonDTO newPerson = await _mediator.Send(command);

            return CreatedAtAction(nameof(Get), new { id = newPerson.Id }, newPerson);
        }

        [HttpGet]
        [Route("{id}")]
        public async Task<IActionResult> Get([FromRoute] Guid id)
        {
            _logger.LogInformation($"{nameof(Get)} - Start");

            return Ok(await _mediator.Send(new GetPersonCommand() { Id = id }));
        }

        [HttpGet]
        public async Task<IActionResult> Filter([FromQuery] GetFilteredPersonCommand query)
        {
            return Ok(await _mediator.Send(query));
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Update([FromRoute] Guid id, [FromBody] UpdatePersonCommand command)
        {
            command.Id = id;
            await _mediator.Send(command);
            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete([FromRoute] Guid id)
        {
            await _mediator.Send(new DeletePersonCommand() { Id = id });
            return NoContent();
        }
    }
}
