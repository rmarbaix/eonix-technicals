﻿using MediatR;
using System;

namespace Person.API.Application.Commands
{
    public class DeletePersonCommand : IRequest
    {
        public Guid Id { get; set; }
    }   
}
