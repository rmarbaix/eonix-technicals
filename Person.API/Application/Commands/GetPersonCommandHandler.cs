﻿using MediatR;
using Microsoft.Extensions.Logging;
using Person.Infrastructure;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Person.API.Application.Commands
{
    public class GetPersonCommandHandler : IRequestHandler<GetPersonCommand, PersonDTO>
    {
        private readonly ILogger<GetPersonCommandHandler> _logger;
        private readonly IMediator _mediator;
        private readonly PersonContext _context;

        public GetPersonCommandHandler(
            ILogger<GetPersonCommandHandler> logger,
            IMediator mediator,
            PersonContext context)
        {
            _logger = logger;
            _mediator = mediator;
            _context = context;
        }

        public async Task<PersonDTO> Handle(GetPersonCommand request, CancellationToken cancellationToken)
        {
            Domain.Entities.Person person = await _context.Persons.FindAsync(request.Id);

            // check if person exist otherwise return KeyNotFoundException
            if (person == null)
            {
                throw new KeyNotFoundException();
            }

            PersonDTO personDTO = new PersonDTO()
            {
                FirstName = person.FirstName,
                LastName = person.LastName,
                Id = person.Id
            };

            return personDTO;
        }
    }
}
