﻿using MediatR;
using System;

namespace Person.API.Application.Commands
{
    public class GetPersonCommand : IRequest<PersonDTO>
    {
        public Guid Id { get; set; }
    }
}
