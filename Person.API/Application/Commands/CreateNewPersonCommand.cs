﻿using MediatR;
using System;

namespace Person.API.Application.Commands
{
    public class CreateNewPersonCommand : IRequest<PersonDTO>
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }   
}
