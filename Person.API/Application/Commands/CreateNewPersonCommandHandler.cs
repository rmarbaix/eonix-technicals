﻿using MediatR;
using Microsoft.Extensions.Logging;
using Person.Infrastructure;
using System.Threading;
using System.Threading.Tasks;

using Entities = Person.Domain.Entities;

namespace Person.API.Application.Commands
{
    public class CreateNewPersonCommandHandler : IRequestHandler<CreateNewPersonCommand, PersonDTO>
    {
        private readonly ILogger<CreateNewPersonCommandHandler> _logger;
        private readonly IMediator _mediator;
        private readonly PersonContext _context;

        public CreateNewPersonCommandHandler(
            ILogger<CreateNewPersonCommandHandler> logger, 
            IMediator mediator,
            PersonContext context)
        {
            _logger = logger;
            _mediator = mediator;
            _context = context;
        }

        public async Task<PersonDTO> Handle(CreateNewPersonCommand request, CancellationToken cancellationToken)
        {
            var newPerson = new Entities.Person(request.FirstName, request.LastName);

            _context.Persons.Add(newPerson);
            await _context.SaveChangesAsync(cancellationToken);

            PersonDTO personDTO = new PersonDTO()
            {
                FirstName = newPerson.FirstName,
                LastName = newPerson.LastName,
                Id = newPerson.Id
            };

            return personDTO;
        }
    }
}
