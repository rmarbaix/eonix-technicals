﻿using MediatR;
using Microsoft.Extensions.Logging;
using Person.Infrastructure;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using Entities = Person.Domain.Entities;

namespace Person.API.Application.Commands
{
    public class UpdatePersonCommandHandler : IRequestHandler<UpdatePersonCommand>
    {
        private readonly ILogger<UpdatePersonCommandHandler> _logger;
        private readonly IMediator _mediator;
        private readonly PersonContext _context;

        public UpdatePersonCommandHandler(
            ILogger<UpdatePersonCommandHandler> logger, 
            IMediator mediator,
            PersonContext context)
        {
            _logger = logger;
            _mediator = mediator;
            _context = context;
        }

        public async Task<Unit> Handle(UpdatePersonCommand request, CancellationToken cancellationToken)
        {
            var currentPerson = await _context.Persons.FindAsync(request.Id);

            // check if person exist otherwise return KeyNotFoundException
            if (currentPerson == null)
            {
                throw new KeyNotFoundException();
            }

            currentPerson.FirstName = request.Firstname;
            currentPerson.LastName = request.LastName;

            await _context.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}
