﻿using MediatR;
using Microsoft.Extensions.Logging;
using Person.Infrastructure;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using Entities = Person.Domain.Entities;

namespace Person.API.Application.Commands
{
    public class DeletePersonCommandHandler : IRequestHandler<DeletePersonCommand>
    {
        private readonly ILogger<CreateNewPersonCommandHandler> _logger;
        private readonly PersonContext _context;

        public DeletePersonCommandHandler(
            ILogger<CreateNewPersonCommandHandler> logger, 
            PersonContext context)
        {
            _logger = logger;
            _context = context;
        }

        public async Task<Unit> Handle(DeletePersonCommand request, CancellationToken cancellationToken)
        {
            var currentPerson = await _context.Persons.FindAsync(request.Id);

            // check if person exist otherwise return KeyNotFoundException
            if (currentPerson == null)
            {
                throw new KeyNotFoundException();
            }

            _context.Persons.Remove(currentPerson);
            await _context.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}
