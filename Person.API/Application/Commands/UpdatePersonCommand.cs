﻿using MediatR;
using System;

namespace Person.API.Application.Commands
{
    public class UpdatePersonCommand : IRequest
    {
        public Guid Id { get; set; }
        public string Firstname { get; set; }
        public string LastName { get; set; }
    }   
}
