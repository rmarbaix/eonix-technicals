﻿using MediatR;
using System.Collections.Generic;

namespace Person.API.Application.Queries
{
    public class GetFilteredPersonCommand : IRequest<List<PersonDTO>>
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
