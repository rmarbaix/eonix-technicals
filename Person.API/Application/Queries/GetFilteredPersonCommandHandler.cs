﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Person.Infrastructure;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Person.API.Application.Queries
{
    public class GetFilteredPersonCommandHandler : IRequestHandler<GetFilteredPersonCommand, List<PersonDTO>>
    {
        private readonly ILogger<GetFilteredPersonCommandHandler> _logger;
        private readonly IMediator _mediator;
        private readonly PersonContext _context;
        private readonly IMapper _mapper;

        public GetFilteredPersonCommandHandler(
            ILogger<GetFilteredPersonCommandHandler> logger,
            IMediator mediator,
            PersonContext context,
            IMapper mapper)
        {
            _logger = logger;
            _mediator = mediator;
            _context = context;
            _mapper = mapper;
        }

        public async Task<List<PersonDTO>> Handle(GetFilteredPersonCommand request, CancellationToken cancellationToken)
        {
            IQueryable<Domain.Entities.Person> query = _context.Persons;
            if (!string.IsNullOrEmpty(request.FirstName))
            {
                query = query.Where(p => p.FirstName.ToLower()
                .StartsWith(request.FirstName.ToLower()) || p.FirstName.ToLower().EndsWith(request.FirstName.ToLower()));
            }

            if (!string.IsNullOrEmpty(request.LastName))
            {
                query = query.Where(p => p.LastName.ToLower()
                .StartsWith(request.LastName.ToLower()) || p.LastName.ToLower().EndsWith(request.LastName.ToLower()));
            }

            // AutoMapper can use

            return await query.Select(x => new PersonDTO() { 
                Id = x.Id,
                FirstName = x.FirstName,
                LastName = x.LastName
            }).ToListAsync(cancellationToken);
        }
    }
}
