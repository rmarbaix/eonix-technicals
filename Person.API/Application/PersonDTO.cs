﻿using System;

namespace Person.API.Application
{
    public record PersonDTO
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
